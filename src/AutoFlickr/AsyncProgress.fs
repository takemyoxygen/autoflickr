﻿namespace AutoFlickr

open System.Threading

type AsyncProgress<'a, 'b, 'c> = {
    Id: 'a;
    Async: Async<'b>;
    Progress: IEvent<'c>
}

module AsyncProgress =
    let create async progress id = {Async = async; Progress = progress; Id = id}

    let dispatchOnCurrentThread progress = 
        let dispatched = new Event<_>()
        let ctx = SynchronizationContext.Current
        progress.Progress |> Event.add (fun p -> 
            if ctx = null then dispatched.Trigger p
            else ctx.Post((fun _ -> dispatched.Trigger p), null))
        { Async = progress.Async; Progress = dispatched.Publish; Id = progress.Id}

    let progress asyncProgress = asyncProgress.Progress

    let asAsync asyncProgress = asyncProgress.Async

    let oneByOne id progressMapping actions  =
        let combinedProgress = new Event<_>()
        actions |> List.iter (fun a -> a.Progress 
                                       |> Event.map (progressMapping a)
                                       |> Event.add combinedProgress.Trigger)
        let asyncs = actions |> List.map (fun a -> a.Async)
        let combinedAsync = async {
            return asyncs |> List.map Async.RunSynchronously
        }

        { Id = id; Async = combinedAsync; Progress = combinedProgress.Publish }




