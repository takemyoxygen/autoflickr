﻿#load "References.fsx"

open AutoFlickr

// this will open a browser, ask for authorizing the application and gives back a verification code.
let requestToken = Authentication.authenticate()

let verificationCode = "481-097-661"

let token = Authentication.getAccessToken requestToken verificationCode

let upload = Upload.fromFolder token @"C:\Temp\to upload" false DoNothing
upload.Tasks |> Array.ofSeq