﻿namespace AutoFlickr

open FlickrNet

type Album = {Id: string; Title: string}

module Albums =

    let private toAlbum (photoset: Photoset) = {Id = photoset.PhotosetId; Title = photoset.Title}
    
    let private toAlbums (photosets: PhotosetCollection) = 
        photosets |> Seq.map toAlbum |> List.ofSeq

    let private handleMapped map (success: _ -> unit) (error: exn -> unit)  (result: FlickrResult<_>) =
        if result.HasError then error result.Error
        else result.Result |> map |> success

    let all token = 
        let client = Flickr.authorizedClient token
        Async.FromContinuations(fun (s, e, _) -> 
            client.PhotosetsGetListAsync(fun r -> handleMapped toAlbums s e r))

    let create token title primaryPhoto = 
        let client = Flickr.authorizedClient token
        Async.FromContinuations(fun (s, e, _) -> 
            client.PhotosetsCreateAsync(title, primaryPhoto, fun r -> 
                handleMapped (fun (a: Photoset) -> {Id = a.PhotosetId; Title = title}) s e r))

    let addPicture token album picture =
        let client = Flickr.authorizedClient token
        Async.FromContinuations(fun (s, e, _) ->
            client.PhotosetsAddPhotoAsync(album, picture, fun r -> handleMapped ignore s e r))