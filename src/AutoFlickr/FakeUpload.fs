﻿namespace AutoFlickr

open System.IO

open FlickrNet

module FakeUpload = 

    let uploadAsync (token: OAuthAccessToken) (file: string) = 
        let pictureId = Path.GetFileNameWithoutExtension file
        let uploading = async{
            printfn "Uploading: %s" file
            return pictureId
        }
        AsyncProgress.create uploading (new Event<int>()).Publish pictureId

    let uploadAndAssign (token: OAuthAccessToken) (file: string) (album: string) (knownAlbums: Album list) =
        let albums = {Id = album; Title = album} :: knownAlbums |> Seq.distinct |> List.ofSeq
        let id = album + "/" + (Path.GetFileNameWithoutExtension file)
        let uploading = async {
            printfn "Uploading %s to \"%s\"" file album
            return id, albums
        }
        AsyncProgress.create uploading (new Event<int>()).Publish id

