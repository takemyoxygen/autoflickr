﻿namespace AutoFlickr

open System
open System.IO

module Files =

    let rec filesFrom folder = seq {
        yield! Directory.GetFiles folder
        yield! Directory.GetDirectories folder
               |> Seq.map filesFrom
               |> Seq.collect id
    }

    let knownFilesFrom folder knownTypes = 
        filesFrom folder
        |> Seq.filter (fun path -> knownTypes |> Set.contains ((Path.GetExtension path).ToLower()) )

    let relativePath (path: string) (absolute: string) = 
        [absolute; Path.DirectorySeparatorChar.ToString(); Path.AltDirectorySeparatorChar.ToString()]
        |> List.fold (fun (s: string) c -> s.Replace(c, String.Empty)) path

    let delete = File.Delete

    let move file (folder: string) = 
        if not(Directory.Exists folder) then
            Directory.CreateDirectory(folder) |> ignore
        let newFile = Path.Combine(folder, Path.GetFileName(file))
        File.Move(file, newFile)

