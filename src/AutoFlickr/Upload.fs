﻿namespace AutoFlickr

open System
open System.Collections.Generic
open System.IO

open FlickrNet

type AfterUpload =
    | DoNothing
    | DeleteFile
    | MoveTo of folder: string

type UploadTask =
    | Upload of filename: string
    | UploadToAlbum of filename: string * album: string

type OverallProgress = 
    | UploadingStarted of filename: string
    | UploadingCompleted of filename: string * uploaded: int * remaining: int * result: OperationResult<string>
    | CreatingAlbumStarted of album: string
    | CreatingAlbumCompleted of album: string
    | AssigningPictureStarted of picture: string * album: string
    | AssigningPictureCompleted of picture: string * album: string
    | DeletingPicture of filename: string
    | MovingPicture of filename: string * folder: string
    | AfterUploadError of msg: string
        
type PicturesUpload = {
    Tasks: UploadTask list;
    Uploading: Async<(UploadTask * OperationResult<string>) list>;
    FileUploadingProgress: IEvent<string*int>;
    OverallProgress: IEvent<OverallProgress>
}

module Upload =

    let knownImageFormats = [".jpg"; ".png"] |> Set.ofSeq

    let private executeAfterUpload file action (overallProgress: Event<OverallProgress>) =
        try
            match action with
            | DoNothing -> ()
            | DeleteFile -> 
                Files.delete file
                overallProgress.Trigger(DeletingPicture file)
            | MoveTo(folder) -> 
                Files.move file folder
                overallProgress.Trigger(MovingPicture(file, folder))
        with
        | e -> overallProgress.Trigger(AfterUploadError e.Message)

    let private appendAlbumName afterUpload album =
        match afterUpload with
        | MoveTo(folder) -> Path.Combine(folder, album) |> MoveTo
        | any -> any

    let private fixProgressPercentage (progress: IEvent<string*int>) =
        let uploading = new HashSet<string>()
        progress |> Event.map (fun (picture, progress) ->
            match progress, (uploading.Contains picture) with
            | _, false ->
                uploading.Add picture |> ignore 
                picture, progress
            | 0, true -> 
                uploading.Remove picture |> ignore
                picture, 100
            | _ -> picture, progress)
        

    let file (token: OAuthAccessToken) (filename: string) (uploadProgress: Event<string*int>) (overallProgress: Event<OverallProgress>) afterUplad =
        let flickr = Flickr.authorizedClient token
        async {
            try
                use _ = flickr.OnUploadProgress.Subscribe(fun p -> uploadProgress.Trigger(filename, p.ProcessPercentage))
                let pictureId = flickr.UploadPicture(filename, null, null, null, false, false, false)
                executeAfterUpload filename afterUplad overallProgress
                return Success(pictureId)
            with
            | e -> return Error(e.Message)
        }

    let private picturesWithAlbums folder = 
        Directory.GetDirectories folder
        |> Array.map(fun subfolder -> Files.relativePath subfolder folder, Files.knownFilesFrom subfolder knownImageFormats)
        |> Seq.collect (fun (album, pictures) -> pictures |> Seq.map (fun p -> UploadToAlbum(p, album)))

    let private plainPictures folder =
        Files.knownFilesFrom folder knownImageFormats
        |> Seq.map Upload

    let private loadAlbumsIfNeeded token knownAlbums = async {
        try
            if (knownAlbums |> List.isEmpty) then
                let! albums = Albums.all token
                return Success(albums)
            else 
                return Success(knownAlbums)
        with
        | e -> return Error(e.Message)
    }

    let private uploadAndAssign token filename album knownAlbums (uploadProgress: Event<string*int>) (overallProgress: Event<OverallProgress>) afterUpload = 
        async {
            let! uploadResult = file token filename uploadProgress overallProgress (appendAlbumName afterUpload album)
            match uploadResult with
            | Success(picture) -> 
                let! albumsResult = loadAlbumsIfNeeded token knownAlbums
                match albumsResult with
                | Success(albums) ->
                    try
                        match knownAlbums |> List.tryFind (fun e -> e.Title.Equals(album, StringComparison.InvariantCultureIgnoreCase)) with
                        | Some(knownAlbum) -> 
                            overallProgress.Trigger(AssigningPictureStarted(filename, album))
                            do! Albums.addPicture token knownAlbum.Id picture
                            overallProgress.Trigger(AssigningPictureCompleted(filename, album))
                            return Success(picture), albums
                        | None -> 
                            overallProgress.Trigger(CreatingAlbumStarted(album))
                            let! newAlbum = Albums.create token album picture
                            overallProgress.Trigger(CreatingAlbumCompleted(album))
                            return Success(picture), (newAlbum :: albums)
                    with
                    | e -> return Error(e.Message), albums
                | Error(e) -> return Error(e), knownAlbums
            | error -> return error, knownAlbums
        }

    let private handle token tasks (uploadProgress: Event<string*int>) (overallProgress: Event<OverallProgress>) afterUpload = 
        let totalTasks = tasks |> List.length
        let rec loop tasks knownAlbums result uploaded = async {
            let reportCompleted file result = overallProgress.Trigger(UploadingCompleted(file, uploaded + 1, totalTasks - uploaded - 1, result))
            match tasks with
            | (Upload filename) as task :: rest ->
                overallProgress.Trigger(UploadingStarted(filename))
                let! uploadResult = file token filename uploadProgress overallProgress afterUpload
                reportCompleted filename uploadResult
                return! loop rest knownAlbums (result @ [task, uploadResult]) (uploaded + 1)
            | UploadToAlbum(filename, album) as task :: rest ->
                overallProgress.Trigger(UploadingStarted(filename))
                let! (uploadResult, albums) = uploadAndAssign token filename album knownAlbums uploadProgress overallProgress afterUpload
                reportCompleted filename uploadResult
                return! loop rest albums (result @ [task, uploadResult]) (uploaded + 1)
            | [] -> return result
        }
        loop tasks [] [] 0

    let fromFolder token folder assignAlbums afterUpload = 
        let tasks = folder
                    |> (if assignAlbums then picturesWithAlbums else plainPictures)
                    |> List.ofSeq

        let uploadProgress = new Event<string*int>()
        let overallProgress = new Event<OverallProgress>()

        let asyncUpload = handle token tasks uploadProgress overallProgress afterUpload

        { Tasks = tasks; 
          Uploading = asyncUpload; 
          OverallProgress = overallProgress.Publish; 
          FileUploadingProgress = uploadProgress.Publish |> fixProgressPercentage }

