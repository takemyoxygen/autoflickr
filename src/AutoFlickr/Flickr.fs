﻿namespace AutoFlickr

open FlickrNet

module Flickr = 

    let createClient() = new Flickr(Parameters.apiKey, Parameters.sharedSecret)

    let authorizedClient (token: OAuthAccessToken) =
        let client = createClient()
        client.OAuthAccessToken <- token.Token
        client.OAuthAccessTokenSecret <- token.TokenSecret
        client

