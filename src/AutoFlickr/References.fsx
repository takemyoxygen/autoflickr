﻿#I "../packages/FlickrNet.3.14.0/lib/net20"
#r "FlickrNet.dll"

// So far, I don't know any other way to make it work 
#load "Parameters.fs"
open AutoFlickr

#load "Flickr.fs"

#load "AsyncProgress.fs"
open AutoFlickr

#load "Files.fs"

#load "Authentication.fs"

#load "Albums.fs"
#load "FakeUpload.fs"

open AutoFlickr
#load "Upload.fs"