﻿namespace AutoFlickr

open System.Diagnostics

open FlickrNet

module Authentication =

    let authenticate() =
        let client = Flickr.createClient()
        let requestToken = client.OAuthGetRequestToken("oob")
        let url = client.OAuthCalculateAuthorizationUrl(requestToken.Token, AuthLevel.Write)
        Process.Start(url) |> ignore
        requestToken

    let getAccessToken requestToken verificationCode = 
        let client = Flickr.createClient()
        let token = client.OAuthGetAccessToken(requestToken, verificationCode)
        token
