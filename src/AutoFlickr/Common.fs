﻿namespace AutoFlickr

type OperationResult<'a> = 
    | Success of result: 'a
    | Error of string