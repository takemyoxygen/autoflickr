﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.

open System
open System.IO

open AutoFlickr

let upload folder afterUpload= 
    let requestToken = Authentication.authenticate()
    printfn "Enter verification code:"
    let verificationCode = Console.ReadLine()
    let accessToken = Authentication.getAccessToken requestToken verificationCode
    printfn "Authenticated as %s" accessToken.Username

    printfn "Starting upload from %s" folder

    let upload = Upload.fromFolder accessToken folder true afterUpload

    printfn "Pictures to upload: %i" upload.Tasks.Length

    upload.OverallProgress
    |> Event.add (function 
                  | UploadingStarted(file) -> printfn "Uploading started: %s" file
                  | UploadingCompleted(file, uploaded, remaining, result) ->
                    let resultString = match result with Success(_) -> "success" | Error(e) -> sprintf "error: %s" e
                    printfn "Upload of %s completed. Result: %s. Uploaded: %i. To upload: %i" file resultString uploaded remaining
                  | CreatingAlbumStarted(album) -> printfn "Creating an album: \"%s\"" album
                  | CreatingAlbumCompleted(album) -> printfn "Album created: \"%s\"" album
                  | AssigningPictureStarted(picture, album) -> printfn "Adding %s to album \"%s\"" picture album
                  | AssigningPictureCompleted(picture, album) -> printfn "%s added to album \"%s\"" picture album
                  | MovingPicture(file, folder) -> printfn "%s was moved to %s" file folder
                  | DeletingPicture(file) -> printfn "%s was deleted" file
                  | AfterUploadError(e) -> printf "An error occured after upload: %s" e)

    upload.FileUploadingProgress |> Event.add (fun (file, progress) -> printfn "%s - %i%%" file progress)

    upload.Uploading |> Async.RunSynchronously |> ignore

    printfn "Uploading completed"

let parseArgs (args: string[]) =
    if (args.Length = 0 || args.Length > 2) then Error("Invalid number of arguments")
    else
        let folder = args.[0]
        if not(Directory.Exists folder) then Error(sprintf "%s does not exist" folder)
        elif args.Length = 1 then Success(folder, DoNothing)
        else
            let second = args.[1].ToLower()
            if second = "-delete" then Success(folder, DeleteFile)
            elif second.StartsWith "-move-to=" then
                let moveTo = second.Replace("-move-to=", String.Empty).Replace("\"", String.Empty)
                try
                    let absolute = Path.GetFullPath moveTo
                    Success(folder, MoveTo(absolute))
                with
                | _ -> Error(sprintf "Invalid path %s" moveTo)
            else Error(sprintf "Unknown option: %s" args.[1])
        

[<EntryPoint>]
let main argv = 
    match parseArgs argv with
    | Success(folder, afterUpload) -> 
        upload folder afterUpload
        Console.ReadKey() |> ignore
    | Error(msg) -> Console.WriteLine msg
    0
